package com.grokkt.android.bonjour;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MDNSDaemon extends Service {
  public MDNSDaemon() {
  }

  private native void _mdnsdStart();

  private native void _mdnsdStop();

  public IBinder onBind(Intent intent) {
    return null;
  }

  public void onCreate() {
    super.onCreate();
    new Thread(new Runnable() {
      @Override
      public void run() {
        _mdnsdStart();
      }
    }).start();
  }

  public void onDestroy() {
    super.onDestroy();
    _mdnsdStop();
  }

  static {
    System.loadLibrary("jdns_sd");
  }
}
