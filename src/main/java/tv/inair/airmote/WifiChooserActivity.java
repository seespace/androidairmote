package tv.inair.airmote;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import inair.eventcenter.proto.Helper;
import inair.eventcenter.proto.Proto;
import tv.inair.airmote.connection.OnEventReceived;
import tv.inair.airmote.connection.OnSocketStateChanged;
import tv.inair.airmote.connection.SocketClient;
import tv.inair.airmote.connection.WifiAP;
import tv.inair.airmote.connection.WifiApContainer;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class WifiChooserActivity extends FragmentActivity
        implements OnEventReceived, OnSocketStateChanged, AdapterView.OnItemClickListener {

  private TextView tvSsid;
  private RelativeLayout rlvConnectHudContainer;
  private TextView tvMessage;
  private TextView tvPleaseWait;

  private AlertDialog mAlertDialog;

  //region Adapter
  private class ListAdapter extends ArrayAdapter<WifiAP> {
    public ListAdapter(Context context, int resourceId) {
      super(context, resourceId);
    }

    /*private view holder class*/
    private class ViewHolder {
      ImageView signalView;
      TextView ssidView;
    }

    public synchronized WifiAP getItem(String ssid, String bssid) {
      if (ssid == null || bssid == null) {
        return null;
      }
//      WifiChooserActivity.this.finish();
//      WifiApContainer.getWifiAps().clear();
      for (int i = 0; i < getCount(); i++) {
        WifiAP item = getItem(i);
        if (ssid.equalsIgnoreCase(item.ssid) && bssid.equalsIgnoreCase(item.bssid)) {
          return item;
        }
      }
      return null;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
      ViewHolder holder;
      WifiAP rowItem = getItem(position);

      if (convertView == null) {
        LayoutInflater mInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.wifi_item, null);
        holder = new ViewHolder();
        holder.ssidView = (TextView) convertView.findViewById(R.id.ssid);
        holder.signalView = (ImageView) convertView.findViewById(R.id.signal);
        convertView.setTag(holder);
      } else {
        holder = (ViewHolder) convertView.getTag();
      }

      if (rowItem.isOpenNetwork()) {
        holder.signalView.setImageResource(R.drawable.wifi_signal_open_light);
      } else {
        holder.signalView.setImageResource(R.drawable.wifi_signal_lock_light);
      }
      holder.signalView.setImageLevel(rowItem.strength);
      holder.ssidView.setText(rowItem.ssid);

      return convertView;
    }
  }
  //endregion

  //region Local Handler
  public static final int MSG_DISMISS_DIALOG = 1;

  private static class LocalHandler extends Handler {
    WeakReference<WifiChooserActivity> mActivity;

    public LocalHandler(WifiChooserActivity activity) {
      mActivity = new WeakReference<>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      if (mActivity == null || mActivity.get() == null || mActivity.get().mDestroyed) {
        return;
      }
      AlertDialog hud = mActivity.get().mHud;
      switch (msg.what) {
        case MSG_DISMISS_DIALOG:
          if (hud != null && hud.isShowing()) {
            hud.dismiss();
          }
          break;

        default:
          break;
      }
    }
  }
  //endregion

  private LocalHandler mHandler;
  private ListAdapter adapter;
  private AlertDialog mHud;
  private ProgressDialog mProgress;
  private AlertDialog mAlert;
  private SocketClient mClient;
  private ListView lvWifiEndpoints;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_wifi_chooser);
    lvWifiEndpoints = (ListView) findViewById(R.id.lv_wifi_endpoints);
    lvWifiEndpoints.setOnItemClickListener(this);
    tvSsid = (TextView) findViewById(R.id.tv_ssid);
    rlvConnectHudContainer = (RelativeLayout) findViewById(R.id.connect_hud_container);
    tvMessage = (TextView) findViewById(R.id.tv_message);
    tvPleaseWait = (TextView) findViewById(R.id.tv_please_wait);

    setResult(RESULT_CANCELED);

    setTitle("Choose network");
    adapter = new ListAdapter(this, R.layout.wifi_item);
    adapter.addAll(WifiApContainer.getWifiAps());
    lvWifiEndpoints.setAdapter(adapter);

    ActionBar actionBar = getActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }

    mProgress = new ProgressDialog(this);
    mProgress.setIndeterminate(true);
    mAlert = new AlertDialog.Builder(this).create();
    mHandler = new LocalHandler(this);

    mClient = Application.getSocketClient();
    mClient.addEventReceivedListener(this);
    mClient.addSocketStateChangedListener(this);

    startScan();
  }

  private void startScan() {
    if (adapter.getCount() > 0) {
      return;
    }
    mAlertDialog = setupHUD("Scanning wifi", true);
    mAlertDialog.show();
    mClient.sendEvent(Helper.setupWifiScanRequest());
  }

  private boolean mDestroyed = false;

  @Override
  protected void onDestroy() {
    if (mAlertDialog != null && mAlertDialog.isShowing()) {
      mAlertDialog.dismiss();
      mAlertDialog = null;
    }
    mDestroyed = true;
    mClient.changeToSettingMode(false);
    WifiApContainer.getWifiAps().clear();
    super.onDestroy();
  }

  public static final String EXTRA_POSITION = "#row_pos";

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    WifiAP item = adapter.getItem(position);
    if (item.isOpenNetwork()) {
      mClient.sendEvent(Helper.setupWifiConnectRequestWithSSID(item.ssid, ""));
      finishSetup(true);
    } else {
      Intent i = new Intent(this, WifiConnectActivity.class);
      i.putExtra(WifiConnectActivity.EXTRA_SSID, item.ssid);
      i.putExtra(EXTRA_POSITION, position);
      startActivityForResult(i, MainFragment.REQUEST_WIFI_CONNECT);
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode != Activity.RESULT_OK) {
      return;
    }
    switch (requestCode) {
      case MainFragment.REQUEST_WIFI_CONNECT:
        int position = data.getIntExtra(EXTRA_POSITION, -1);
        if (adapter.getCount() <= 0) {
          adapter.addAll(WifiApContainer.getWifiAps());
        }

        if (position < 0 || position >= adapter.getCount()) {
          return;
        }

        WifiAP item = adapter.getItem(data.getIntExtra(EXTRA_POSITION, -1));
        if (item == null) {
          return;
        }

        item.password = data.getStringExtra(WifiConnectActivity.EXTRA_PASSWORD);
//        setupHUD("Connecting to " + item.ssid, true).show();
        setupConnectHud(true, item.ssid);
        mClient.sendEvent(Helper.setupWifiConnectRequestWithSSID(item.ssid, item.password));
        break;
    }
  }

  private void setupConnectHud(boolean connecting, String ssid) {
    rlvConnectHudContainer.setVisibility(View.VISIBLE);
    lvWifiEndpoints.setVisibility(View.INVISIBLE);

    if (connecting) {
      tvMessage.setText("Joining ");
      tvSsid.setText(ssid);
      tvPleaseWait.setVisibility(View.VISIBLE);
    } else {
      tvMessage.setText("Connected to ");
//      tvSsid.setText(ssid);
      tvPleaseWait.setVisibility(View.GONE);
    }
  }

  final List<WifiAP> mItems = new ArrayList<>();

  @Override
  public void onEventReceived(Proto.Event event) {
    if (isFinishing() || mDestroyed || event == null || event.type == null
            || event.type != Proto.Event.SETUP_RESPONSE) {
      return;
    }

    Proto.SetupResponseEvent responseEvent = event.getExtension(Proto.SetupResponseEvent.event);

    switch (responseEvent.phase) {
      case Proto.REQUEST_WIFI_SCAN:
        if (responseEvent.wifiNetworks == null || responseEvent.wifiNetworks.length == 0) {
          mAlertDialog = setupHUD("No wifi available", false);
          mAlertDialog.show();
          mItems.clear();
          adapter.clear();
          WifiApContainer.getWifiAps().clear();
          return;
        }

        synchronized (mItems) {
          for (Proto.WifiNetwork wifi : responseEvent.wifiNetworks) {
            WifiAP item = adapter.getItem(wifi.ssid, wifi.bssid);
            if (item == null) {
              item = new WifiAP();
              item.ssid = wifi.ssid;
              item.bssid = wifi.bssid;
              mItems.add(item);
              adapter.add(item);
              WifiApContainer.addAp(item);
            }
            item.strength = wifi.strength;
            item.capabilities = wifi.capabilities;
            item.notRemove = true;
          }

          for (int i = 0; i < mItems.size(); ++i) {
            WifiAP item = mItems.get(i);
            if (!item.notRemove) {
              mItems.remove(i);
              adapter.remove(item);
              WifiApContainer.getWifiAps().remove(i);
              --i;
            }
          }
        }
        if (mHud != null && mHud.isShowing()) {
          mHud.dismiss();
        }
        break;

      case Proto.REQUEST_WIFI_CONNECT:
        if (mHud != null && mHud.isShowing()) {
          mHud.dismiss();
        }
        if (responseEvent.error) {
          mAlertDialog = setupHUD(responseEvent.errorMessage, false);
          mAlertDialog.show();
        } else {
          finishSetup(true);
        }

        rlvConnectHudContainer.setVisibility(View.INVISIBLE);
        lvWifiEndpoints.setVisibility(View.VISIBLE);
        break;
    }
  }

  @Override
  public void onStateChanged(boolean connect, String message) {
    if (mDestroyed) {
      return;
    }
    System.out.println("WifiChooserActivity.onStateChanged " + connect + " " + message);
    if (!connect) {
      adapter.clear();
      mItems.clear();
      WifiApContainer.getWifiAps().clear();
      if (mHud != null && mHud.isShowing()) {
        mHud.dismiss();
      }
    } else {
      startScan();
    }
  }

  private AlertDialog setupHUD(String mes, boolean isProgress) {
    if (mHud != null) {
      mHud.dismiss();
    }
    AlertDialog dialog = isProgress ? mProgress : mAlert;
    dialog.setMessage(mes);
    System.out.println("WifiChooserActivity.setupHUD " + mes);
    mHud = dialog;
    return mHud;
  }

  private void finishSetup(boolean ok) {
    if (mDestroyed) {
      return;
    }
    if (ok) {
      setupConnectHud(false, null);
      WifiChooserActivity.this.finish();
      WifiApContainer.getWifiAps().clear();
    } else {
      mAlertDialog = setupHUD("Fail to setup InAiR Device, please go back to main screen and try again", false);
      rlvConnectHudContainer.setVisibility(View.INVISIBLE);
      lvWifiEndpoints.setVisibility(View.VISIBLE);
    }

    setResult(ok ? RESULT_OK : RESULT_CANCELED);
  }

  @Override
  public void onBackPressed() {
    rlvConnectHudContainer.setVisibility(View.INVISIBLE);
    lvWifiEndpoints.setVisibility(View.VISIBLE);
    WifiChooserActivity.this.finish();
    WifiApContainer.getWifiAps().clear();
    super.onBackPressed();
  }
}
