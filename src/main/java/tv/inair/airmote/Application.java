package tv.inair.airmote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.grokkt.android.bonjour.MDNSDaemon;

import java.lang.ref.WeakReference;

import io.fabric.sdk.android.Fabric;
import tv.inair.airmote.connection.NsdDiscoverHelper;
import tv.inair.airmote.connection.SocketClient;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class Application extends android.app.Application implements android.app.Application.ActivityLifecycleCallbacks {

  public static final boolean DEBUG = false;

  public static final String USB_DEVICE_NAME = "InAir USB";

  public static final String SETUP_WIFI_CMD = "https://www.inair.tv/setupwifi";

  //public static final String FIRST_TIME_KEY = "#first_time";
  //private static final String TAG = "Application";

  private static SocketClient mSocketClient;

  public static SocketClient getSocketClient() {
    return mSocketClient;
  }

  private static final String TEMP = "#temp";
  private static SharedPreferences mTempPreferences;

  public static SharedPreferences getTempPreferences() {
    return mTempPreferences;
  }

  private static SharedPreferences mDefaultSharedPreferences;
  public static SharedPreferences getDefaultPreferences() {
    return mDefaultSharedPreferences;
  }

  public static final int ERROR_COLOR = Color.parseColor("#ffff4444");
  public static final int NORMAL_COLOR = Color.parseColor("#ffffffff");
  public static final int SUCCESS_COLOR = Color.parseColor("#ff33b5e5");

  private static Application sInstance;

  public enum Status {
    ERROR,
    NORMAL,
    SUCCESS
  }

  private static String mCurrentMessage = "Disconnected";
  private static Status mCurrentType = Status.ERROR;

  private static WeakReference<TextView> mStatus;

  public static void setStatusView(TextView status) {
    if (status != null) {
      mStatus = new WeakReference<>(status);
    }

    notify(mCurrentMessage, mCurrentType);
  }

  public static void notify(String message, Status type) {
    mCurrentType = type;
    mCurrentMessage = message;

    if (mStatus != null && mStatus.get() != null) {
      TextView view = mStatus.get();
      view.setText(mCurrentMessage);
      view.setTypeface(view.getTypeface(), Typeface.NORMAL);
      switch (mCurrentType) {
        case ERROR:
          view.setTextColor(Color.WHITE);
          view.setBackgroundColor(ERROR_COLOR);
          break;

        case NORMAL:
          view.setTextColor(Color.BLACK);
          view.setBackgroundColor(NORMAL_COLOR);
          break;

        case SUCCESS:
          view.setTextColor(Color.WHITE);
          view.setBackgroundColor(SUCCESS_COLOR);
          break;
      }
    }
  }

  private WifiManager.MulticastLock mMulticastLock;

  @Override
  public void onCreate() {
    super.onCreate();
    if (!BuildConfig.DEBUG) {
      Fabric.with(this, new Crashlytics());
    }

    WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
    mMulticastLock = wifiManager.createMulticastLock("sample_lock");
    mMulticastLock.setReferenceCounted(true);
    mMulticastLock.acquire();
    startService(new Intent(this, MDNSDaemon.class));

    sInstance = Application.this;
    registerActivityLifecycleCallbacks(this);
    mSocketClient = new SocketClient(Application.this);

    // Clear temporary preferences
    mTempPreferences = getSharedPreferences(TEMP, Context.MODE_PRIVATE);
    getTempPreferences().edit().clear().commit();

    mDefaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(Application.this);
  }

  @Override
  public void onTerminate() {
    super.onTerminate();

    if (mMulticastLock != null) {
      mMulticastLock.release();
    }
    stopService(new Intent(this, MDNSDaemon.class));
  }

  public static Application getInstance() {
    return sInstance;
  }

  private static DisplayMetrics _metrics = null;
  public static float unifyPixelsValues(float px) {
    if (_metrics == null) {
      _metrics = getInstance().getResources().getDisplayMetrics();
    }
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_IN, px, _metrics);
  }

  private int started = 0;
  private int created = 0;

  @Override
  public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    created++;
  }

  @Override
  public void onActivityStarted(Activity activity) {
    started++;
    if (activity instanceof MainActivity) {
      mSocketClient.connectToDeviceIfHave(activity);
    }
  }

  @Override
  public void onActivityResumed(Activity activity) {
    if (mSocketClient != null) {
      mSocketClient.setForegroundActivity(activity);
    }
  }

  @Override
  public void onActivityPaused(Activity activity) {
  }

  @Override
  public void onActivityStopped(Activity activity) {
    started--;
    if (started == 0) {
      if (mSocketClient != null) {
        mSocketClient.disconnect(false);
      }
    }
  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
  }

  @Override
  public void onActivityDestroyed(Activity activity) {
    created--;
    if (created == 0) {
      NsdDiscoverHelper.sharedInstance().stopScan();
    }
  }
}
