package tv.inair.airmote;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import inair.eventcenter.proto.Helper;
import inair.eventcenter.proto.Proto;
import tv.inair.airmote.connection.OnEventReceived;
import tv.inair.airmote.connection.OnSocketStateChanged;
import tv.inair.airmote.connection.SocketClient;
import tv.inair.airmote.remote.GestureControl;
import tv.inair.airmote.utils.CircleIndicator;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class MainFragment extends Fragment implements View.OnClickListener, OnEventReceived, OnSocketStateChanged, GestureControl.Listener {

  public static final int REQUEST_WIFI_SETUP = 1;
  public static final int REQUEST_WIFI_CONNECT = 2;
  public static final int REQUEST_SCAN_INAIR = 10;
  public static final int REQUEST_TEXT_INPUT = 20;

  private static final String TAG = "MainFragment";
  private GestureControl mGestureControl;
  private View mRootView;
  private View mControlView;
  private View mControlContainer;

  private FrameLayout mGuideViewContainer;

  private SocketClient mClient;
  private boolean webViewShown = false;
  private boolean textInputShown = false;
  private boolean isDeviceListShowing = false;

  //region Override parent
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    mClient = Application.getSocketClient();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);

    mRootView = view.findViewById(R.id.rootView);
    mControlView = view.findViewById(R.id.controlView);
    mControlContainer = view.findViewById(R.id.controlContainer);
    mGuideViewContainer = ((FrameLayout) view.findViewById(R.id.imgv_guide_view_container));
    ViewPager mGuidePager = (ViewPager) view.findViewById(R.id.guide_view_pager);
    CircleIndicator mCircleIndicator = (CircleIndicator) view.findViewById(R.id.pager_indicator);
    mCircleIndicator.setViewPager(mGuidePager);

    view.findViewById(R.id.helpBtn).setOnClickListener(this);
    view.findViewById(R.id.moreBtn).setOnClickListener(this);
    view.findViewById(R.id.scanBtn).setOnClickListener(this);
    view.findViewById(R.id.threeDBtn).setOnClickListener(this);
    view.findViewById(R.id.settingBtn).setOnClickListener(this);

    ViewTreeObserver vto = mRootView.getViewTreeObserver();
    vto.addOnGlobalLayoutListener(
      new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
          ViewTreeObserver obs = mRootView.getViewTreeObserver();
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            obs.removeOnGlobalLayoutListener(this);
          } else {
            obs.removeGlobalOnLayoutListener(this);
          }
          mGuideViewContainer.setVisibility(View.GONE);

          hideControlView();
        }
      }
    );

    return view;
  }

  @Override
  public void onStart() {
    super.onStart();

    mGestureControl = new GestureControl(getActivity(), getView());
    mGestureControl.setListener(this);
    mClient.addEventReceivedListener(this);
    mClient.addSocketStateChangedListener(this);
  }

  @Override
  public void onResume() {
    super.onResume();
    hideControlView();
    webViewShown = false;
    textInputShown = false;
  }

  @Override
  public void onDestroyView() {
    mGestureControl.setListener(null);
    super.onDestroyView();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    isDeviceListShowing = false;
    if (resultCode != Activity.RESULT_OK) {
      mClient.updateCurrentStatus();
      return;
    }
    switch (requestCode) {
      case REQUEST_WIFI_SETUP:
        break;

      case REQUEST_SCAN_INAIR:
        break;
    }
  }

  @Override
  public void onStop() {
//    onBackPressed();
    super.onStop();
  }

  public boolean onBackPressed() {
    if (mGuideViewContainer.getVisibility() == View.VISIBLE) {
      mGuideViewContainer.setVisibility(View.GONE);
      mClient.changeToSettingMode(false);
      return true;
    }
    return false;
  }
  //endregion

  //region Buttons
  private void showHelp() {
    Intent i = new Intent(getActivity(), WebViewActivity.class);
    i.putExtra(WebViewActivity.EXTRA_URL, getResources().getString(R.string.help_url));
    startActivity(i);
  }

  private boolean isShow = true;

  private void toggleControlView() {
    if (isShow) {
      hideControlView();
    } else {
      showControlView();
    }
  }

  private void showControlView() {
    isShow = true;
    mControlView.animate().translationY(0).setDuration(250);
  }

  private void hideControlView() {
    isShow = false;
    mControlView.animate().translationY(mControlContainer.getHeight()).setDuration(250);
  }

  private void handleScanDevices() {
    Intent i = new Intent(getActivity(), DeviceListActivity.class);
    startActivityForResult(i, REQUEST_SCAN_INAIR);
    isDeviceListShowing = true;
  }

  private void switchDisplayMode() {
    mClient.sendEvent(Helper.newFunctionEvent(Proto.FunctionEvent.F4));
  }

  private void settingDevice() {
    mGuideViewContainer.setVisibility(View.VISIBLE);
    mClient.changeToSettingMode(true);

    if (mClient.isUsingAoaUsb() && mClient.isConnected()) {
      mGuideViewContainer.setVisibility(View.GONE);
      Intent i = new Intent(getActivity(), WifiChooserActivity.class);
      startActivityForResult(i, REQUEST_WIFI_SETUP);
    }
  }
  //endregion

  //region Implement
  private void processTextInput(Proto.Event event) {
    if (textInputShown || getActivity() == null) {
      return;
    }

    Proto.TextInputRequestEvent textInputEvent = event.getExtension(Proto.TextInputRequestEvent.event);
    assert textInputEvent != null;
    Intent i = new Intent(getActivity(), TextInputActivity.class);
    i.putExtra(TextInputActivity.EXTRA_REPLY_TO, event.replyTo);
    i.putExtra(TextInputActivity.EXTRA_MAX_LENGTH, textInputEvent.maxLength);
    startActivityForResult(i, REQUEST_TEXT_INPUT);
    textInputShown = true;
  }

  private void processWebView(Proto.Event event, boolean isWebView) {
    if (webViewShown || getActivity() == null) {
      return;
    }

    Intent i = new Intent(getActivity(), WebViewActivity.class);
    i.putExtra(WebViewActivity.EXTRA_REPLY_TO, event.replyTo);

    if (isWebView) {
      Proto.WebViewRequestEvent webViewEvent = event.getExtension(Proto.WebViewRequestEvent.event);
      assert webViewEvent != null;

      if (webViewEvent.url.equals(Application.SETUP_WIFI_CMD)) {
        settingDevice();
        return;
      }

      i.putExtra(WebViewActivity.EXTRA_URL, webViewEvent.url);
      i.putExtra(WebViewActivity.EXTRA_IS_WEBVIEW, true);
    } else {
      Proto.OAuthRequestEvent oAuthEvent = event.getExtension(Proto.OAuthRequestEvent.event);
      assert oAuthEvent != null;
      i.putExtra(WebViewActivity.EXTRA_URL, oAuthEvent.authUrl);
    }

    startActivity(i);
    webViewShown = true;
  }

  @Override
  public void onEventReceived(Proto.Event event) {
    if (mClient.isInSettingMode()) {
      return;
    }

    if (event != null && event.type != null) {
      if (!mClient.isUsingAoaUsb()) {
        mClient.ensureNotConnectToWifiDirect();
      }

      switch (event.type) {
        case Proto.Event.WEBVIEW_REQUEST:
          Log.d(TAG, "Handling open web view");
          processWebView(event, true);
          break;

        case Proto.Event.OAUTH_REQUEST:
          processWebView(event, false);
          break;

        case Proto.Event.TEXT_INPUT_REQUEST:
          processTextInput(event);
          break;
      }
    }
  }

  @Override
  public void onStateChanged(boolean connect, String message) {
    if (!isVisible()) {
      return;
    }
    if (mGuideViewContainer.getVisibility() == View.VISIBLE) {
      if (connect) {
        mGuideViewContainer.setVisibility(View.GONE);
        if (mClient.isInSettingMode()) {
          Intent i = new Intent(getActivity(), WifiChooserActivity.class);
          startActivityForResult(i, REQUEST_WIFI_SETUP);
        }
      }
    }
  }

  @Override
  public void onEvent() {
    hideControlView();

    if (!mClient.isConnecting() && !mClient.isConnected() && !isDeviceListShowing) {
      handleScanDevices();
    }
  }

  @Override
  public void onClick(View v) {
    if (v == null || v.getId() == View.NO_ID) {
      return;
    }

    switch (v.getId()) {
      case R.id.helpBtn:
        showHelp();
        return;

      case R.id.moreBtn:
        toggleControlView();
        return;

      case R.id.scanBtn:
        handleScanDevices();
        break;

      case R.id.threeDBtn:
        switchDisplayMode();
        break;

      case R.id.settingBtn:
        settingDevice();
        break;
    }

    hideControlView();
  }
  //endregion
}
