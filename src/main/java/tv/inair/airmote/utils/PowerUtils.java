package tv.inair.airmote.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/17/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class PowerUtils {
  public static boolean isConnected(Context context, boolean includeAc) {
    Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    if (intent != null) {
      int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
      return (includeAc && plugged == BatteryManager.BATTERY_PLUGGED_AC) || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }
    return false;
  }
}
