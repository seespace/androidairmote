package tv.inair.airmote.utils;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/17/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class LocalHandler extends Handler {
  public interface CanHandleMessage {
    void handleMessage(Message msg);
  }

  WeakReference<CanHandleMessage> mClient;

  public LocalHandler(CanHandleMessage client) {
    mClient = new WeakReference<>(client);
  }

  @Override
  public void handleMessage(Message msg) {
    CanHandleMessage client = mClient.get();
    if (client == null) {
      return;
    }
    client.handleMessage(msg);
  }
}