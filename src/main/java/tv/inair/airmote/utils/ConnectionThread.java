package tv.inair.airmote.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/17/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public abstract class ConnectionThread extends Thread implements LocalHandler.CanHandleMessage {

  private static final int MSG_CANCEL_THREAD = 1;

  public ConnectionThread(String name) {
    super(name);
  }

  public final void cancel() {
    if (!checkIfRunInLocalHandler()) {
      getMessageForLocalHandler(MSG_CANCEL_THREAD, 0, 0).sendToTarget();
      return;
    }
    doCancel();
  }

  public final void handleMessage(Message msg) {
    switch (msg.what) {
      case MSG_CANCEL_THREAD:
        cancel();
        break;
      default:
        handleLocalMessage(msg);
    }
  }

  protected abstract void handleLocalMessage(Message msg);

  protected abstract void doCancel();

  private final Handler mLocalHandler = new LocalHandler(this);

  protected final boolean checkIfRunInLocalHandler() {
    return Looper.myLooper() == mLocalHandler.getLooper();
  }

  protected final Message getMessageForLocalHandler(int what, int arg1, int arg2) {
    return Message.obtain(mLocalHandler, what, arg1, arg2);
  }
}
