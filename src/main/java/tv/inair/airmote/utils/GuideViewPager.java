package tv.inair.airmote.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import tv.inair.airmote.R;

/**
 * Created by nguyencongtrung on 7/28/15.
 */
public class GuideViewPager extends ViewPager {

  private int[] resIds = new int[]{
          R.drawable.before_you_begin_1,
          R.drawable.before_you_begin_2,
          R.drawable.before_you_begin_3
  };

  public GuideViewPager(Context context) {
    super(context);
    init(context);
  }

  public GuideViewPager(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  private void init(Context context) {
    if (context instanceof FragmentActivity) {
      setOffscreenPageLimit(0);
      FragmentActivity fragmentActivity = (FragmentActivity) context;
      final ImagePagerAdapter pagerAdapter = new ImagePagerAdapter(fragmentActivity.getSupportFragmentManager(), resIds.length);
      setAdapter(pagerAdapter);

      setOnPageChangeListener(new OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//          GuideFragment guideFragment = (GuideFragment) pagerAdapter.getRegisteredFragment(position);
//          guideFragment.loadImage();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
      });
    }
  }

  private class ImagePagerAdapter extends FragmentStatePagerAdapter {
    private final int mSize;

    public ImagePagerAdapter(FragmentManager fm, int size) {
      super(fm);
      mSize = size;
    }

    @Override
    public int getCount() {
      return mSize;
    }

    @Override
    public Fragment getItem(int position) {
      return GuideFragment.newInstance(resIds[position]);
    }
  }

}
