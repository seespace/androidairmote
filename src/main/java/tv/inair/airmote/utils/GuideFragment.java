package tv.inair.airmote.utils;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import tv.inair.airmote.R;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by nguyencongtrung on 7/28/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class GuideFragment extends Fragment {

  private static final String EXT_RES_ID = "ext_res_id";
  private ImageView mGuideImageView;
  private int mResId;
  private ImageLoader mImageLoader;

  public static GuideFragment newInstance(int resId) {
    GuideFragment guideFragment = new GuideFragment();

    final Bundle args = new Bundle();
    args.putInt(EXT_RES_ID, resId);
    guideFragment.setArguments(args);

    return guideFragment;
  }

  public GuideFragment() {
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mResId = getArguments() != null ? getArguments().getInt(EXT_RES_ID) : -1;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.fragment_guide, container, false);
    mGuideImageView = (ImageView) view.findViewById(R.id.imgv_guide_view);
    mImageLoader = ImageLoader.sharedInstance(getActivity().getApplicationContext());

    if (mResId != -1) {
      ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
      if (viewTreeObserver != null) {
        viewTreeObserver.addOnGlobalLayoutListener(
          new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
              ViewTreeObserver observer = view.getViewTreeObserver();
              mImageLoader.loadBitmap(mResId, mGuideImageView);
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                observer.removeOnGlobalLayoutListener(this);
              } else {
                observer.removeGlobalOnLayoutListener(this);
              }
            }
          }
        );
      }
    }

    return view;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (mGuideImageView != null) {
      mImageLoader.cancelWork(mGuideImageView);
      mGuideImageView.setImageDrawable(null);
    }
  }
}
