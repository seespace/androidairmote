package tv.inair.airmote.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Process;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by nguyencongtrung on 7/28/15.
 */
public class ImageLoader {

  private final Context mContext;
  private LruCache<String, BitmapDrawable> mMemoryCache;
  private static final int FADE_IN_TIME = 200;

  private static ImageLoader instance;

  public static ImageLoader sharedInstance(Context context) {
    if (instance == null) {
      instance = new ImageLoader(context);
    }

    return instance;
  }

  private ImageLoader(Context context) {
    mContext = context;
    initMemCache();
  }

  public void loadBitmap(int resId, ImageView imageView) {
    final String imageKey = String.valueOf(resId);

    final  BitmapDrawable bitmapDrawable = getBitmapFromMemCache(imageKey);
    if (bitmapDrawable != null && bitmapDrawable.getBitmap() != null && !bitmapDrawable.getBitmap().isRecycled()) {
      setImageDrawable(imageView, bitmapDrawable);
    } else if (cancelPotentialWork(resId, imageView)) {
      final BitmapWorkerTask task = new BitmapWorkerTask(resId, imageView);
      final AsyncDrawable asyncDrawable =
              new AsyncDrawable(mContext.getResources(), null, task);
      imageView.setImageDrawable(asyncDrawable);
      task.execute();
    }
  }

  class BitmapWorkerTask extends AsyncTask<Void, Void, BitmapDrawable> {
    private final WeakReference<ImageView> imageViewReference;
    private int mData = 0;
    private int mReqWidth;
    private int mReqHeight;

    public BitmapWorkerTask(int data, ImageView imageView) {
      // Use a WeakReference to ensure the ImageView can be garbage collected
      imageViewReference = new WeakReference<ImageView>(imageView);
      mData = data;
      mReqWidth = imageView.getMeasuredWidth();
      mReqHeight = imageView.getMeasuredHeight();
    }

    // Decode image in background.
    @Override
    protected BitmapDrawable doInBackground(Void... params) {
      android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
      final Bitmap bitmap = decodeSampledBitmapFromResource(
              mContext.getResources(), mData, mReqWidth, mReqHeight);
      RecyclingBitmapDrawable bitmapDrawable = new RecyclingBitmapDrawable(mContext.getResources(), bitmap);
      addBitmapToMemoryCache(String.valueOf(mData), bitmapDrawable);
      return bitmapDrawable;
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(BitmapDrawable value) {
      if (isCancelled()) {
        value = null;
      }

      if (imageViewReference != null && value != null) {
        final ImageView imageView = imageViewReference.get();
        if (imageView != null) {
          setImageDrawable(imageView, value);
        }
      }
    }
  }

  private void setImageDrawable(ImageView imageView, Drawable drawable) {
    // Transition drawable with a transparent drawable and the final drawable
    final TransitionDrawable td = new TransitionDrawable(
            new Drawable[]{new ColorDrawable(android.R.color.transparent), drawable});

    imageView.setImageDrawable(td);
    td.startTransition(FADE_IN_TIME);
  }

  class AsyncDrawable extends BitmapDrawable {
    private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

    public AsyncDrawable(Resources res, Bitmap bitmap,
                         BitmapWorkerTask bitmapWorkerTask) {
      super(res, bitmap);
      bitmapWorkerTaskReference =
              new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
    }

    public BitmapWorkerTask getBitmapWorkerTask() {
      return bitmapWorkerTaskReference.get();
    }
  }

  private void initMemCache() {
    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    final int cacheSize = maxMemory / 8;

    mMemoryCache = new LruCache<String, BitmapDrawable>(cacheSize) {
      @Override
      protected int sizeOf(String key, BitmapDrawable bitmapDrawable) {
        // The cache size will be measured in kilobytes rather than
        // number of items.
        return getBitmapSize(bitmapDrawable);
      }

      @Override
      protected void entryRemoved(boolean evicted, String key, BitmapDrawable oldValue, BitmapDrawable newValue) {
        if (RecyclingBitmapDrawable.class.isInstance(oldValue)) {
          ((RecyclingBitmapDrawable) oldValue).setIsCached(false);
        }
      }
    };
  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  public static int getBitmapSize(BitmapDrawable value) {
    Bitmap bitmap = value.getBitmap();

    // From KitKat onward use getAllocationByteCount() as allocated bytes can potentially be
    // larger than bitmap byte count.
    if (Utils.hasKitKat()) {
      return bitmap.getAllocationByteCount();
    }

    if (Utils.hasHoneycombMR1()) {
      return bitmap.getByteCount();
    }

    // Pre HC-MR1
    return bitmap.getRowBytes() * bitmap.getHeight();
  }

  public void addBitmapToMemoryCache(String key, BitmapDrawable bitmap) {
    if (getBitmapFromMemCache(key) == null) {
      mMemoryCache.put(key, bitmap);
    }
  }

  public BitmapDrawable getBitmapFromMemCache(String key) {
    return mMemoryCache.get(key);
  }

  public void cancelWork(ImageView imageView) {
    final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
    if (bitmapWorkerTask != null) {
      bitmapWorkerTask.cancel(true);
    }
  }

  private boolean cancelPotentialWork(int data, ImageView imageView) {
    final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

    if (bitmapWorkerTask != null) {
      final int bitmapData = bitmapWorkerTask.mData;
      // If bitmapData is not yet set or it differs from the new data
      if (bitmapData == 0 || bitmapData != data) {
        // Cancel previous task
        bitmapWorkerTask.cancel(true);
      } else {
        // The same work is already in progress
        return false;
      }
    }
    // No task associated with the ImageView, or an existing task was cancelled
    return true;
  }

  private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
    if (imageView != null) {
      final Drawable drawable = imageView.getDrawable();
      if (drawable instanceof AsyncDrawable) {
        final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
        return asyncDrawable.getBitmapWorkerTask();
      }
    }
    return null;
  }

  private Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                 int reqWidth, int reqHeight) {

    // First decode with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ALPHA_8;
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeResource(res, resId, options);

    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
    try {
      return BitmapFactory.decodeResource(res, resId, options);
    } catch (OutOfMemoryError error) {
      return Bitmap.createBitmap(reqWidth, reqHeight, Bitmap.Config.ALPHA_8);
    }
  }

  private int calculateInSampleSize(
          BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

      final int halfHeight = height / 2;
      final int halfWidth = width / 2;

      // Calculate the largest inSampleSize value that is a power of 2 and keeps both
      // height and width larger than the requested height and width.
      while ((halfHeight / inSampleSize) > reqHeight
              && (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }

    return inSampleSize;
  }
}
