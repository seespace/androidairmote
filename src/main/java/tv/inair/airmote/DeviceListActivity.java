package tv.inair.airmote;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import tv.inair.airmote.connection.BaseConnection;
import tv.inair.airmote.connection.NsdDiscoverHelper;
import tv.inair.airmote.connection.SocketClient;

/**
 * This Activity appears as a dialog. It lists any paired devices and
 * devices detected in the area after discovery. When a device is chosen
 * by the user, the MAC address of the device is sent back to the parent
 * Activity in the result Intent.
 */
public class DeviceListActivity extends ListActivity implements NsdDiscoverHelper.DevicesChangedListener {
  private ArrayAdapter<String> mAdapter;
  private SocketClient mClient;
  private NsdDiscoverHelper mDiscoverHelper;
  private List<String> mDevicesName = new ArrayList<>();
  private Handler mHandler = new Handler();

  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    if (position == mAdapter.getCount() - 1) {
      // show dialog to inform user enter InAir ip address to connect manually
      final AlertDialog.Builder builder = new AlertDialog.Builder(DeviceListActivity.this);
      builder.setTitle(R.string.please_enter_your_ia_ip);
      final EditText ipInput = new EditText(DeviceListActivity.this);
      ipInput.setHint(R.string.ip_input_hint);
      builder.setView(ipInput);

      builder.setPositiveButton(R.string.ok_btn, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          if (mClient.isUsingAoaUsb()) {
            mClient.switchToWifiConnection();
          }
          // connect to specified ip address
          String enteredIpAddress = ipInput.getText().toString().trim();
          BaseConnection.Device device = new BaseConnection.Device(enteredIpAddress, enteredIpAddress);
          mClient.connectTo(device);
          finish();
        }
      });

      builder.setNegativeButton(R.string.cancel_btn, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
      });

      builder.show();
    } else {
      if (mClient.isUsingAoaUsb()) {
        mClient.switchToWifiConnection();
      }
      mClient.connectTo(mDiscoverHelper.getDeviceAtPosition(position));
      finish();
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setResult(Activity.RESULT_CANCELED);
    mDiscoverHelper = NsdDiscoverHelper.sharedInstance();

    DisplayMetrics metrics = getResources().getDisplayMetrics();
    final int screenHeight = metrics.heightPixels;

    ViewTreeObserver vto = getListView().getViewTreeObserver();
    vto.addOnGlobalLayoutListener(
            new ViewTreeObserver.OnGlobalLayoutListener() {
              @Override
              public void onGlobalLayout() {
                ViewTreeObserver obs = getListView().getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                  obs.removeOnGlobalLayoutListener(this);
                } else {
                  obs.removeGlobalOnLayoutListener(this);
                }

//                getListView().setDividerHeight(4);
                getListView().setDivider(getResources().getDrawable(R.drawable.list_view_divider));
                getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, (int) (screenHeight * 0.5));
              }
            }
    );

    mClient = Application.getSocketClient();
//    mAdapter = new ArrayAdapter<>(this, R.layout.device_item, mDevicesName);
    mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mDevicesName);
    mAdapter.add(getResources().getString(R.string.other));
    setListAdapter(mAdapter);

    setTitle("Scanning");
    Application.notify("Scanning ...", Application.Status.NORMAL);
    mDiscoverHelper.startScan(DeviceListActivity.this);

    mHandler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (mClient != null && !mClient.isConnecting() && !mClient.isConnected() && !mClient.isConnectionEstablished()) {
          BaseConnection.Device device = mDiscoverHelper.getDeviceIfHasOnlyOne();
          if (device != null) {
            mClient.connectTo(device);
            finish();
          }
        }
      }
    }, 2000);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
//    mDiscoverHelper.unregisterListener();
    mDiscoverHelper.stopScan();
  }

  @Override
  public void onDevicesChanged(List<String> devicesName) {
//    mAdapter.clear();
//    mAdapter.addAll(devicesName);
//    mAdapter.add(getResources().getString(R.string.other));
    mDevicesName.clear();
    mDevicesName.addAll(devicesName);
    mDevicesName.add(getResources().getString(R.string.other));
    mAdapter.notifyDataSetChanged();
  }
}
