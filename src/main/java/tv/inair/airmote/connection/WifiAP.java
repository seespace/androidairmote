package tv.inair.airmote.connection;

/**
 * Created by nguyencongtrung on 8/6/15.
 */
public class WifiAP {
  public String ssid;
  public int strength;
  public String bssid;
  public String capabilities;

  public String password;

  public boolean isOpenNetwork() {
    return !capabilities.contains("PSK") && !capabilities.contains("WEP");
  }

  public boolean notRemove;
}
