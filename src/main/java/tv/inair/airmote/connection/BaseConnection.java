package tv.inair.airmote.connection;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import inair.eventcenter.proto.Helper;
import inair.eventcenter.proto.Proto;
import tv.inair.airmote.Application;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public abstract class BaseConnection {

  private static final String TAG = "BaseConnection";

  public BaseConnection() {
    super();
  }

  public static class Device {
    public static final Device EMPTY = new Device(null, null);

    public String deviceName;

    public byte[] inetAddr = null;
    public String address = null;
    String domain;

    public Parcelable parcelable;

    public Device(String deviceName, String address) {
      this.deviceName = deviceName;
      this.address = address;
    }

    @Override
    public String toString() {
      return "Device {" +
        "deviceName='" + deviceName + '\'' +
        ", address='" + address + '\'' +
        ", inet='" + Arrays.toString(inetAddr) + '\'' +
        '}';
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof Device)) {
        return false;
      }

      Device device = (Device) o;

      if (deviceName != null ? !deviceName.equals(device.deviceName) : device.deviceName != null) {
        return false;
      }
      return !(address != null ? !address.equals(device.address) : device.address != null);

    }

    @Override
    public int hashCode() {
      int result = deviceName != null ? deviceName.hashCode() : 0;
      result = 31 * result + (address != null ? address.hashCode() : 0);
      return result;
    }

    private static final String HOST_NAME_KEY = "#hostname";
    private static final String DISPLAY_NAME_KEY = "#displayname";

    public static void saveToPref(SharedPreferences preferences, Device device) {
      preferences.edit()
        .putString(HOST_NAME_KEY, device.address)
        .putString(DISPLAY_NAME_KEY, device.deviceName)
        .apply();
    }

    public static Device getFromPref(SharedPreferences preferences) {
      return new Device(preferences.getString(DISPLAY_NAME_KEY, null), preferences.getString(HOST_NAME_KEY, null));
    }
  }

  public interface DeviceDiscoverListener {
    void onDeviceFound(Device device);

    void onDeviceLost(Device device);
  }

  public interface Constants {
    // Message types sent from the BluetoothChatService Handler
    int MESSAGE_STATE_CHANGE = 1;
    int MESSAGE_READ = 2;
    int MESSAGE_DEVICE_NAME = 4;

    // Key names received from the BluetoothChatService Handler
    String DEVICE_NAME = "device_name";
    String DEVICE_ADDRESS = "device_address";
  }

  //region Connection State
  // Constants that indicate the current connection state
  public static final int STATE_NONE = 0;                   // we're doing nothing
  public static final int STATE_FAIL = 1;                   // we're doing nothing
  public static final int STATE_CONNECTING = 2;             // now initiating an outgoing connection
  public static final int STATE_ESTABLISH = 3;
  public static final int STATE_CONNECTED = 4;  // now connected to a remote device
  private volatile int mState = STATE_NONE;

  private synchronized void setState(int state) {
    if (state != mState) {
      Log.i(TAG, "State changed to: " + state);
      mState = state;
      if (mHandler != null) {
        mHandler.obtainMessage(BaseConnection.Constants.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
      }
    }
  }

  /**
   * Indicate that the connection attempt failed and notify the UI Activity.
   */
  protected void connecting() {
    setState(STATE_CONNECTING);
  }

  protected void connected() {
    setState(STATE_CONNECTED);
  }

  protected void connectionFail() {
    setState(STATE_FAIL);
  }

  protected void connectionLost() {
    setState(STATE_NONE);
  }

  protected void connectionEstablish() {
    setState(STATE_ESTABLISH);
  }

  public boolean hasConnection() {
    return mState > STATE_FAIL;
  }

  public boolean isConnected() {
    return mState == STATE_CONNECTED;
  }

  public boolean isConnecting() {
    return mState == STATE_CONNECTING;
  }

  public boolean isEstablish() {
    return mState == STATE_ESTABLISH;
  }

  protected boolean canWrite() {
    return mState == STATE_CONNECTED || mState == STATE_ESTABLISH;
  }

  private byte[] mHandShakeData = null;
  protected void handShake() {
    if (mHandShakeData == null) {
      mHandShakeData = Helper.dataFromEvent(Helper.newDeviceEvent(Application.getInstance(), Proto.DeviceEvent.REGISTER));
    }
    write(mHandShakeData);
  }
  //endregion

  //region Handler
  protected Handler mHandler;

  public void setHandler(Handler handler) {
    this.mHandler = handler;
  }
  //endregion

  private WeakReference<DeviceDiscoverListener> mDeviceDiscoverListener = null;

  public final void registerDeviceFoundListener(DeviceDiscoverListener listener) {
    if (listener != null) {
      mDeviceDiscoverListener = new WeakReference<>(listener);
    } else {
      mDeviceDiscoverListener = null;
    }
  }

  protected final void onDeviceFound(final Device device) {
    if (mHandler != null) {
      mHandler.post(
        new Runnable() {
          @Override
          public void run() {
            if (mDeviceDiscoverListener != null && mDeviceDiscoverListener.get() != null) {
              mDeviceDiscoverListener.get().onDeviceFound(device);
            }
          }
        }
      );
    }
  }

  protected final void onDeviceLost(final Device device) {
    if (mHandler != null) {
      mHandler.post(
        new Runnable() {
          @Override
          public void run() {
            if (mDeviceDiscoverListener != null && mDeviceDiscoverListener.get() != null) {
              mDeviceDiscoverListener.get().onDeviceLost(device);
            }
          }
        }
      );
    }
  }

  public void register(Context context) {
  }

  public void unregister(Context context) {
  }

  public abstract boolean startQuickConnect();

  public abstract boolean stopQuickConnect();

  public abstract void startScan(boolean quickConnect);

  public abstract void stopScan();

  public abstract boolean connect(Device device);

  public abstract void stop(boolean all);

  public abstract void write(byte[] data);
}
