package tv.inair.airmote.connection;

import java.util.ArrayList;
import java.util.List;

import tv.inair.airmote.Application;

/**
 * Created by nguyencongtrung on 7/8/15.
 */
public class NsdDiscoverHelper {
  private static NsdDiscoverHelper sharedInstance;

  private List<BaseConnection.Device> mDiscoveredDevices = new ArrayList<>();
  private List<String> mDevicesName = new ArrayList<>();
  private SocketClient mSocketClient = Application.getSocketClient();

  private DevicesChangedListener mListener;

  private NsdDiscoverHelper() {

  }

  public static NsdDiscoverHelper sharedInstance() {
    if (sharedInstance == null) {
      sharedInstance = new NsdDiscoverHelper();
    }
    return sharedInstance;
  }

  public void startScan(DevicesChangedListener listener) {
    mListener = listener;
    mSocketClient.startScanInAir(mDiscoverListener);
  }

  public void stopScan() {
    mSocketClient.stopScanInAir();
    mListener = null;
    mDevicesName.clear();
    mDiscoveredDevices.clear();
  }

  public BaseConnection.Device getDeviceAtPosition(int position) {
    return position >= 0 && position < mDiscoveredDevices.size() ? mDiscoveredDevices.get(position) : null;
  }

  public BaseConnection.Device getDeviceIfHasOnlyOne() {
    return mDiscoveredDevices.size() == 1 ? mDiscoveredDevices.get(0) : null;
  }

  private BaseConnection.DeviceDiscoverListener mDiscoverListener = new BaseConnection.DeviceDiscoverListener() {
    @Override
    public void onDeviceFound(BaseConnection.Device device) {
      if (!mDevicesName.contains(device.deviceName)) {
        mDevicesName.add(device.deviceName);
        mDiscoveredDevices.add(device);
        if (mListener != null) {
          mListener.onDevicesChanged(mDevicesName);
        }
      } else {
        int dupIdx = mDevicesName.indexOf(device.deviceName);
        if (dupIdx >= 0) {
          mDiscoveredDevices.set(dupIdx, device);
        }
      }
    }

    @Override
    public void onDeviceLost(BaseConnection.Device device) {
      if (mDevicesName.contains(device.deviceName)) {
        int lostIndex = mDevicesName.indexOf(device.deviceName);
        if (lostIndex >= 0) {
          mDevicesName.remove(lostIndex);
          mDiscoveredDevices.remove(lostIndex);
          if (mListener != null) {
            mListener.onDevicesChanged(mDevicesName);
          }
        }
      }
    }
  };

  public interface DevicesChangedListener {
    void onDevicesChanged(List<String> devicesName);
  }

}
