package tv.inair.airmote.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.apple.dnssd.DNSSD;
import com.apple.dnssd.DNSSDException;
import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.TXTRecord;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

import tv.inair.airmote.Application;
import tv.inair.airmote.utils.ConnectionThread;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public final class WifiAdapter extends BaseConnection {

  private static final String TAG = "WifiAdapter";

  static {
    System.loadLibrary("jdns_sd");
  }

  //region Singleton
  private static WifiAdapter _instance;

  public static WifiAdapter getInstance() {
    if (_instance == null) {
      _instance = new WifiAdapter();
    }
    return _instance;
  }

  private WifiAdapter() {
    INAIR_WIFI_CONFIG.SSID = INAIR_SSID;

    INAIR_WIFI_CONFIG.status = WifiConfiguration.Status.DISABLED;
    INAIR_WIFI_CONFIG.priority = 40;

    INAIR_WIFI_CONFIG.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
    INAIR_WIFI_CONFIG.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
    INAIR_WIFI_CONFIG.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
    INAIR_WIFI_CONFIG.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
    INAIR_WIFI_CONFIG.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
    INAIR_WIFI_CONFIG.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
    INAIR_WIFI_CONFIG.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
    INAIR_WIFI_CONFIG.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
    INAIR_WIFI_CONFIG.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

    INAIR_WIFI_CONFIG.preSharedKey = INAIR_PASSWORD;

    mNsdHelper = new NSDHelper();
  }
  //endregion

  //region Wifi
  private static final WifiConfiguration INAIR_WIFI_CONFIG = new WifiConfiguration();
  private static final String INAIR_SSID = String.format("\"%s\"", "InAiR");
  private static final String INAIR_PASSWORD = String.format("\"%s\"", "12345678");

  public static final Device INAIR_DEVICE = new Device("InAiR", "192.168.49.1");
  private static final int INAIR_PORT = 8989;

  private ConnectThread mConnectThread;
  private ConnectedThread mConnectedThread;

  private WifiManager mManager;
  private NSDHelper mNsdHelper;

  private SocketClient mClient;

  private boolean mFindWifi = false;
  private boolean mConnectInAiR = false;

  public static boolean isConnecting(SupplicantState state) {
    switch (state) {
      case AUTHENTICATING:
      case ASSOCIATING:
      case ASSOCIATED:
      case FOUR_WAY_HANDSHAKE:
      case GROUP_HANDSHAKE:
      case COMPLETED:
        return true;
      case DISCONNECTED:
      case INTERFACE_DISABLED:
      case INACTIVE:
      case SCANNING:
      case DORMANT:
      case UNINITIALIZED:
      case INVALID:
        return false;
      default:
        throw new IllegalArgumentException("Unknown supplicant state");
    }
  }

  private void _connectToInAiR() {
    if (!mSetupInAiR) {
      stop(true);
      mSetupInAiR = true;
      if (!mManager.isWifiEnabled()) {
        mManager.setWifiEnabled(true);
      }
      mNsdHelper.stopDiscover(true);
      WifiInfo wifiInfo = mManager.getConnectionInfo();
      if (wifiInfo != null && isConnecting(wifiInfo.getSupplicantState()) && _checkIfInAiR(wifiInfo.getSSID())) {
        System.out.println("WifiAdapter._connectToInAiR");
        connect(INAIR_DEVICE);
      } else {
        mFindWifi = true;
        mManager.startScan();
      }
    }
  }

  private void _disableInAirNetwork() {
    int curIAWifi = -1;
    WifiInfo wifiInfo = mManager.getConnectionInfo();
    if (wifiInfo != null && isConnecting(wifiInfo.getSupplicantState()) && _checkIfInAiR(wifiInfo.getSSID())) {
      curIAWifi = wifiInfo.getNetworkId();
    }
    if (INAIR_WIFI_CONFIG.networkId != -1 || curIAWifi != -1) {
      mSetupInAiR = false;
      mNsdHelper.stopDiscover(true);
      mManager.setWifiEnabled(true);
      mManager.disconnect();
      if (curIAWifi != -1) {
        mManager.removeNetwork(curIAWifi);
      }
      if (INAIR_WIFI_CONFIG.networkId != -1) {
        mManager.removeNetwork(INAIR_WIFI_CONFIG.networkId);
      }
      for (WifiConfiguration wifi : mManager.getConfiguredNetworks()) {
        mManager.enableNetwork(wifi.networkId, false);
      }
      mManager.reconnect();
      mManager.saveConfiguration();
      INAIR_WIFI_CONFIG.networkId = -1;
      startScan(true);
    }
  }

  private boolean _checkIfInAiR(String ssid) {
    return INAIR_SSID.equals(ssid);
  }

  private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();

      switch (action) {
        case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
          if (mSetupInAiR && mFindWifi) {
            //System.out.println(" ** " + action);
            List<ScanResult> results = mManager.getScanResults();
            for (ScanResult res : results) {
              if (_checkIfInAiR("\"" + res.SSID + "\"")) {
                mFindWifi = false;
                int netId = mManager.addNetwork(INAIR_WIFI_CONFIG);
                if (netId != -1) {
                  INAIR_WIFI_CONFIG.networkId = netId;
                  mManager.disconnect();
                  mManager.enableNetwork(netId, true);
                } else {
                  Log.d(TAG, "WTF");
                }
                //System.out.println("FOUND INAIR " + netId + " " + INAIR_WIFI_CONFIG.networkId);
                break;
              }
            }
          }
          break;

        case WifiManager.NETWORK_STATE_CHANGED_ACTION:
          NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
          if (networkInfo.isConnected()) {
            if (mSetupInAiR) {
              if (!mFindWifi) {
                WifiInfo wifiInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                //System.out.println(" ++ " + action + " " + mFindWifi + " " + wifiInfo.getSSID() + " " + mConnectInAiR);
                if ("<unknown ssid>".equals(wifiInfo.getSSID())) {
                  return;
                }
                if (_checkIfInAiR(wifiInfo.getSSID())) {
                  if (!mConnectInAiR) {
                    mConnectInAiR = true;
                    mClient.connectTo(INAIR_DEVICE);
                  }
                } else {
                  mFindWifi = true;
                  mManager.startScan();
                }
              }
            } else {
              mNsdHelper.startDiscover();
            }
          }
          break;
      }
    }
  };

  private synchronized void connected(Socket socket, Device device) {
    Log.d(TAG, "Connected " + device);

    mNsdHelper.stopDiscover(true);

    // Cancel the thread that completed the connection
    if (mConnectThread != null) {
      mConnectThread.cancel();
      mConnectThread = null;
    }

    // Cancel any thread currently running a connection
    if (mConnectedThread != null) {
      mConnectedThread.cancel();
      mConnectedThread = null;
    }

    // Start the thread to manage the connection and perform transmissions
    mConnectedThread = new ConnectedThread(socket);
    connectionEstablish();
    handShake();
  }
  //endregion

  //region Implement

  @Override
  public void register(Context context) {
    mClient = Application.getSocketClient();
    mManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

    IntentFilter filter = new IntentFilter();
    filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
    filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
    context.getApplicationContext().registerReceiver(mReceiver, filter);

    if (!mManager.isWifiEnabled()) {
      mManager.setWifiEnabled(true);
    }
  }

  @Override
  public void unregister(Context context) {
    try {
      context.getApplicationContext().unregisterReceiver(mReceiver);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private boolean mQuickConnect = false;
  private boolean mSetupInAiR = false;

  @Override
  public boolean startQuickConnect() {
    if (mClient == null) {
      Log.w(TAG, "WTF???");
      return false;
    }
    if (mClient.isInSettingMode()) {
      _connectToInAiR();
    } else {
      startScan(true);
    }
    return true;
  }

  @Override
  public boolean stopQuickConnect() {
    _disableInAirNetwork();
    return true;
  }

  @Override
  public void startScan(boolean quickConnect) {
    mQuickConnect = quickConnect;
    mNsdHelper.startDiscover();
  }

  @Override
  public void stopScan() {
    mNsdHelper.stopDiscover(true);
  }

  @Override
  protected void connectionFail() {
    System.out.println("WifiAdapter.connectionFail");
    mConnectInAiR = false;
    if (mClient != null && mClient.isInSettingMode() && mSetupInAiR) {
      _connectToInAiR();
    } else {
      super.connectionFail();
    }
  }

  @Override
  protected void connectionLost() {
    System.out.println("WifiAdapter.connectionLost");
    mConnectInAiR = false;
    if (!(mClient != null && mClient.isInSettingMode() && mSetupInAiR)) {
      super.connectionLost();
    }
  }

  @Override
  public synchronized boolean connect(Device device) {
    if (device == null) {
      return false;
    }

    if (device.deviceName.equals(Application.USB_DEVICE_NAME)) {
      return false;
    }

    if (device.address == null && device.inetAddr == null) {
      Log.i(TAG, "Try resolving service");
      mNsdHelper.resolveAndConnectDevice(device);
      return false;
    }

    if (mConnectedThread != null) {
      mConnectedThread.cancel();
      mConnectedThread = null;
    }

    if (mConnectThread != null) {
      //if (!mConnectThread.isInterrupted() && device.equals(mConnectThread.mDevice)) {
      //  return false;
      //}
      mConnectThread.cancel();
      mConnectThread = null;
    }

    if (INAIR_DEVICE == device) {
      mConnectInAiR = true;
    }

    // Send the name of the connected device back to the UI Activity
    Bundle bundle = new Bundle();
    bundle.putString(BaseConnection.Constants.DEVICE_NAME, device.deviceName);
    bundle.putString(Constants.DEVICE_ADDRESS, device.address);
    Message msg = mHandler.obtainMessage(BaseConnection.Constants.MESSAGE_DEVICE_NAME);
    msg.setData(bundle);
    mHandler.sendMessage(msg);

    connecting();
    mConnectThread = new ConnectThread(device);
    mConnectThread.start();
    return true;
  }

  @Override
  public void stop(boolean all) {
    System.out.println("WifiAdapter.stop");

    if (mConnectedThread != null) {
      mConnectedThread.cancel();
      mConnectedThread = null;
    }

    if (mConnectThread != null) {
      mConnectThread.cancel();
      mConnectThread = null;
    }

    connectionLost();
  }

  @Override
  public void write(byte[] data) {
    ConnectedThread r;
    synchronized (this) {
      if (canWrite()) {
        r = mConnectedThread;
        if (r != null) {
          r.write(data);
        }
      }
    }
  }
  //endregion

  //region Thread
  private class ConnectThread extends ConnectionThread {
    public static final int MSG_SUCCESS = 20;
    public static final int MSG_FAIL = 21;

    private Device mDevice;
    private Socket mSocket;

    public ConnectThread(Device device) {
      super("Connect Thread");
      mDevice = device;
    }

    @Override
    public void run() {
      connecting();
      Log.i(TAG, "BEGIN ConnectThread " + mDevice);

      boolean ok = false;
      try {
        InetAddress addr = mDevice.inetAddr != null ? InetAddress.getByAddress(mDevice.inetAddr) : InetAddress.getByName(mDevice.address);
        System.out.println("ConnectThread.run " + addr);
        if (addr.isReachable(20000)) {
          mSocket = new Socket(addr, INAIR_PORT);
          mSocket.setTcpNoDelay(true);
          mDevice.inetAddr = addr.getAddress();
          mDevice.address = addr.getHostAddress();
          ok = true;
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      getMessageForLocalHandler(ok ? MSG_SUCCESS : MSG_FAIL, 0, 0).sendToTarget();
    }

    @Override
    protected void handleLocalMessage(Message msg) {
      switch (msg.what) {
        case MSG_FAIL:
          connectionFail();
          closeSocket();
          break;

        case MSG_SUCCESS:
          connected(mSocket, mDevice);
          break;
      }
    }

    public void doCancel() {
      interrupt();
      Log.i(TAG, "CANCEL ConnectThread ");
    }

    private void closeSocket() {
      if (mSocket != null) {
        try {
          mSocket.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private class ConnectedThread extends ConnectionThread {
    public static final int MSG_FAIL = 10;
    public static final int MSG_ESTABLISH_FAIL = 20;
    public static final int MSG_ESTABLISH_SUCCESS = 21;

    private final Socket mmSocket;

    public ConnectedThread(Socket socket) {
      super("Connected Thread");
      mmSocket = socket;
    }

    @Override
    public void run() {
      Log.i(TAG, "BEGIN ConnectedThread");
      // Get the Socket input and output streams
      try {
        DataInputStream is = new DataInputStream(mmSocket.getInputStream());
        byte[] buffer = new byte[4096];
        int length;

        // Keep listening to the InputStream while connected
        while (!isInterrupted() && mmSocket.isConnected()) {
          length = is.readInt();
          if (length > 0) {
            is.readFully(buffer, 0, length);

            // Send the obtained bytes to the UI Activity
            mHandler.obtainMessage(BaseConnection.Constants.MESSAGE_READ, length, -1, Arrays.copyOf(buffer, length)).sendToTarget();
          }
        }
      } catch (IOException e) {
        if (!isInterrupted()) {
          e.printStackTrace();
          getMessageForLocalHandler(MSG_FAIL, 0, 0).sendToTarget();
        }
      }
    }

    /**
     * Write to the connected OutStream.
     *
     * @param buffer The bytes to write
     */
    public void write(byte[] buffer) {
      try {
        final OutputStream os = mmSocket.getOutputStream();
        os.write(buffer);
        os.flush();
        if (isEstablish()) {
          getMessageForLocalHandler(MSG_ESTABLISH_SUCCESS, 0, 0).sendToTarget();
        }
      } catch (IOException e) {
        Log.e(TAG, "Exception during write", e);
        getMessageForLocalHandler(isEstablish() ? MSG_ESTABLISH_SUCCESS : MSG_FAIL, 0, 0).sendToTarget();
      }
    }

    @Override
    protected void handleLocalMessage(Message msg) {
      switch (msg.what) {
        case MSG_FAIL:
          cancel();
          connectionLost();
          break;

        case MSG_ESTABLISH_SUCCESS:
          if (!isAlive()) {
            start();
          }
          connected();
          break;

        case MSG_ESTABLISH_FAIL:
          cancel();
          connectionFail();
          break;
      }
    }

    public void doCancel() {
      Log.d(TAG, "ConnectedThread.cancel");
      interrupt();
      try {
        mmSocket.close();
      } catch (IOException e) {
        Log.e(TAG, "close() of connect socket failed", e);
      }
    }
  }
  //endregion

  private class NSDHelper {
    private static final String TAG = "NsdHelper";
    private static final String SERVICE_TYPE = "_irpc._tcp.";

    private final Handler mLocalHandler = new Handler(Looper.getMainLooper());

    private boolean isScanning = false;
    private DNSSDService mBrowser;

    public void startDiscover() {
      if (isScanning || mBrowser != null) {
        stopDiscover(true);
      }

      try {
        mBrowser = DNSSD.browse(SERVICE_TYPE, new BrowseListener());
        isScanning = true;
      } catch (DNSSDException e) {
        e.printStackTrace();
      }
    }

    public void stopDiscover(boolean stopAll) {
      if (stopAll) {
        if (mBrowser != null) {
          mBrowser.stop();
          mBrowser = null;
        }
      }
      isScanning = false;
    }

    public void resolveAndConnectDevice(Device device) {
      try {
        DNSSD.resolve(0, DNSSD.ALL_INTERFACES, device.deviceName, SERVICE_TYPE, device.domain, new ResolveListener(device));
      } catch (DNSSDException e) {
        e.printStackTrace();
        connectionFail();
      }
    }

    private class BrowseListener implements com.apple.dnssd.BrowseListener {

      @Override
      public void serviceFound(DNSSDService browser, int flags, int ifIndex, String serviceName, String regType, String domain) {
        mBrowser = browser;
        Device device = new Device(serviceName, null);
        device.domain = domain;
        if (mQuickConnect) {
          mQuickConnect = false;
          stopDiscover(true);
          resolveAndConnectDevice(device);
        } else {
          onDeviceFound(device);
        }
      }

      @Override
      public void serviceLost(DNSSDService browser, int flags, int ifIndex, String serviceName, String regType, String domain) {
        mBrowser = browser;
        Device device = new Device(serviceName, null);
        onDeviceLost(device);
      }

      @Override
      public void operationFailed(DNSSDService service, int errorCode) {
        service.stop();
        Log.e(TAG, "Service browse failed " + errorCode);
      }
    }

    private class ResolveListener implements com.apple.dnssd.ResolveListener {

      Device currentDevice;

      public ResolveListener(Device device) {
        currentDevice = device;
      }

      @Override
      public void serviceResolved(DNSSDService resolver, int flags, int ifIndex, String fullName, String hostName, int port, TXTRecord txtRecord) {
        resolver.stop();
        try {
          DNSSD.queryRecord(flags, ifIndex, hostName, 1, 1, new QueryAddListener(currentDevice));
        } catch (DNSSDException e) {
          e.printStackTrace();
          connectionFail();
        }
      }

      @Override
      public void operationFailed(DNSSDService service, int errorCode) {
        service.stop();
        connectionFail();
        Log.e(TAG, "Resolve failed" + errorCode);
      }
    }

    private class QueryAddListener implements com.apple.dnssd.QueryListener {

      Device device;

      public QueryAddListener(Device device) {
        this.device = device;
      }

      @Override
      public void queryAnswered(DNSSDService query, int flags, int ifIndex, String fullName, int rrtype, int rrclass, byte[] rdata, int ttl) {
        query.stop();
        device.inetAddr = rdata;
        mLocalHandler.post(
          new Runnable() {
            @Override
            public void run() {
              connect(device);
            }
          }
        );
      }

      @Override
      public void operationFailed(DNSSDService service, int errorCode) {
        service.stop();
        connectionFail();
        Log.e(TAG, "Query failed " + errorCode);
      }
    }
  }
}
