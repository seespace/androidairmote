package tv.inair.airmote.connection;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Arrays;

import inair.eventcenter.proto.Helper;
import inair.eventcenter.proto.Proto;
import tv.inair.airmote.Application;
import tv.inair.airmote.utils.ConnectionThread;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by nguyencongtrung on 7/20/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class AoaUsbAdapter extends BaseConnection {

  private static final String TAG = "AoaUsbAdapter";
  private ConnectedThread mConnectedThread;

  private static final String MANUFACTURE = "SeeSpace";
  private static final String MODEL_NAME = "InAir";

  private static final String ACTION_USB_PERMISSION = "tv.inair.airmote.ACCESSORY_PERMISSION";
  private BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();

      if (action.equals(ACTION_USB_PERMISSION)) {
        boolean permissionGranted = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false);
        if (permissionGranted) {
          UsbAccessory accessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);

          if (accessory != null && isValidAccessory(accessory)) {
            openConnection(accessory);
          }
        }
      }
    }
  };

  public boolean isValidAccessory(UsbAccessory accessory) {
    return (accessory.getManufacturer().equals(MANUFACTURE) && accessory.getModel().equals(MODEL_NAME));
  }

  private static AoaUsbAdapter sInstance;

  public static AoaUsbAdapter getInstance() {
    if (sInstance == null) {
      sInstance = new AoaUsbAdapter();
    }

    return sInstance;
  }

  private AoaUsbAdapter() {
  }

  @Override
  public void register(Context context) {
    IntentFilter intentFilter = new IntentFilter(ACTION_USB_PERMISSION);
    context.getApplicationContext().registerReceiver(mReceiver, intentFilter);
  }

  @Override
  public void unregister(Context context) {
    context.getApplicationContext().unregisterReceiver(mReceiver);
  }

  @Override
  public boolean startQuickConnect() {
    //    UsbAccessory[] accessories = mUsbManager.getAccessoryList();
    //    if (accessories == null || accessories.length <= 0) {
    //      return false;
    //    }
    //
    //    BaseConnection.Device newDevice = new BaseConnection.Device(Application.USB_DEVICE_NAME, "");
    //    newDevice.parcelable = accessories[0];
    //    return connect(newDevice);
    return false;
  }

  @Override
  public boolean stopQuickConnect() {
    return false;
  }

  @Override
  public void startScan(boolean quickConnect) {
  }

  @Override
  public void stopScan() {
  }

  @Override
  public boolean connect(Device device) {
    if (!device.deviceName.equals(Application.USB_DEVICE_NAME)) {
      return false;
    }

    Log.i(TAG, "Connecting");
    if (device.parcelable != null && device.parcelable instanceof UsbAccessory) {
      UsbAccessory accessory = (UsbAccessory) device.parcelable;
      if (isValidAccessory(accessory)) {
        connecting();
        UsbManager usb = (UsbManager) Application.getInstance().getSystemService(Context.USB_SERVICE);
        if (usb.hasPermission(accessory)) {
          openConnection(accessory);
          return true;
        } else {
          usb.requestPermission(accessory, PendingIntent.getBroadcast(Application.getInstance(), 0, new Intent(ACTION_USB_PERMISSION), 0));
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public void stop(boolean all) {
    Log.i(TAG, "Stopping aoa Usb connection");
    if (Application.DEBUG) {
      Toast.makeText(Application.getInstance(), "Stopping USB " + all, Toast.LENGTH_SHORT).show();
    }

    if (all) {
      if (mConnectedThread != null) {
        mConnectedThread.cancel();
        mConnectedThread = null;
      }
    } else {
      if (mConnectedThread != null) {
        mConnectedThread.pauseConnection(true);
      }
    }

    connectionLost();
  }

  @Override
  public void write(byte[] data) {
    ConnectedThread r;
    synchronized (this) {
      if (canWrite()) {
        r = mConnectedThread;
        r.write(data);
      }
    }
  }

  private void openConnection(UsbAccessory accessory) {
    Log.i(TAG, "Try opening connection");

    if (mConnectedThread != null && !accessory.equals(mConnectedThread.mAccessory)) {
      mConnectedThread.cancel();
      mConnectedThread = null;
    }

    if (mConnectedThread == null) {
      if (Application.DEBUG) {
        Toast.makeText(Application.getInstance(), "Try opened NEW", Toast.LENGTH_SHORT).show();
      }
      UsbManager usb = (UsbManager) Application.getInstance().getSystemService(Context.USB_SERVICE);
      ParcelFileDescriptor pfd = usb.openAccessory(accessory);
      if (pfd != null) {
        mConnectedThread = new ConnectedThread(accessory, pfd);
        connectionEstablish();
        handShake();
        Log.i(TAG, "Connection opened successful");
        if (Application.DEBUG) {
          Toast.makeText(Application.getInstance(), "Connection open successful", Toast.LENGTH_SHORT).show();
        }
        return;
      } else {
        if (Application.DEBUG) {
          Toast.makeText(Application.getInstance(), "Connection open fail", Toast.LENGTH_SHORT).show();
        }
      }
      return;
    } else {
      if (mConnectedThread.isAlive()) {
        if (Application.DEBUG) {
          Toast.makeText(Application.getInstance(), "Try UN-PAUSE", Toast.LENGTH_SHORT).show();
        }
        mConnectedThread.pauseConnection(false);
        connected();
        return;
      } else {
        if (Application.DEBUG) {
          Toast.makeText(Application.getInstance(), "THREAD STOPPED", Toast.LENGTH_SHORT).show();
        }
      }
    }
    connectionFail();
  }

  private class ConnectedThread extends ConnectionThread {
    public static final int MSG_FAIL = 10;
    public static final int MSG_ESTABLISH_FAIL = 20;
    public static final int MSG_ESTABLISH_SUCCESS = 21;

    public UsbAccessory mAccessory;
    private WriteThread mWriteThread;
    private InputStream mInputStream;
    private OutputStream mOutputStream;
    private ParcelFileDescriptor mPfd;

    ConnectedThread(UsbAccessory accessory, ParcelFileDescriptor pfd) {
      super("Connected Thread");
      mAccessory = accessory;
      mPfd = pfd;
      mInputStream = new ParcelFileDescriptor.AutoCloseInputStream(mPfd);
      mOutputStream = new BufferedOutputStream(new ParcelFileDescriptor.AutoCloseOutputStream(mPfd));

      mWriteThread = new WriteThread("WriteThread", Process.THREAD_PRIORITY_BACKGROUND);
      mWriteThread.start();
      mWriteThread.prepareHandler();
    }

    private byte[] cachedBuffer;
    private int remainingLength = 0;
    private static final int SIZE_OF_INT = 4;

    @Override
    public void run() {
      byte[] buffer = new byte[4096];

      while (!interrupted()) {
        try {
          int packetLength = mInputStream.read(buffer);
          if (packetLength > 0) {

            int manipulatedLength = 0;

            if (remainingLength > 0) {
              if (remainingLength <= packetLength) {
                byte[] remainingMsBytes = Arrays.copyOfRange(buffer, manipulatedLength, remainingLength);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                outputStream.write(cachedBuffer);
                outputStream.write(remainingMsBytes);

                byte[] msBytes = outputStream.toByteArray();
                outputStream.close();
                mHandler.obtainMessage(BaseConnection.Constants.MESSAGE_READ, msBytes.length, -1, msBytes).sendToTarget();
                remainingLength = 0;
                cachedBuffer = null;
                manipulatedLength += remainingLength;
              } else {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                outputStream.write(cachedBuffer);
                outputStream.write(buffer, 0, packetLength);

                cachedBuffer = outputStream.toByteArray();
                outputStream.close();
                remainingLength -= packetLength;
              }
            }

            while (remainingLength == 0) {
              byte[] intArr = Arrays.copyOfRange(buffer, manipulatedLength, manipulatedLength + SIZE_OF_INT);
              int msLength = new BigInteger(intArr).intValue();
              manipulatedLength += SIZE_OF_INT;

              if ((manipulatedLength + msLength) > packetLength) {
                if (manipulatedLength < packetLength) {
                  cachedBuffer = Arrays.copyOfRange(buffer, manipulatedLength, packetLength);
                  remainingLength = manipulatedLength + msLength - packetLength;
                } else {
                  cachedBuffer = null;
                  remainingLength = 0;
                }
                break;
              } else {
                if (!mIsPause) {
                  byte[] msBytes = Arrays.copyOfRange(buffer, manipulatedLength, manipulatedLength + msLength);
                  mHandler.obtainMessage(BaseConnection.Constants.MESSAGE_READ, msLength, -1, msBytes).sendToTarget();
                }
                manipulatedLength += msLength;
                remainingLength = 0;
              }
            }
          }
        } catch (IOException ignore) {
          getMessageForLocalHandler(MSG_FAIL, 0, 0).sendToTarget();
          break;
        }
      }
    }

    public void write(final byte[] data) {
      mWriteThread.runTask(
        new Runnable() {
          @Override
          public void run() {
            try {
              mOutputStream.write(data);
              mOutputStream.flush();
              if (isEstablish()) {
                getMessageForLocalHandler(MSG_ESTABLISH_SUCCESS, 0, 0).sendToTarget();
              }
            } catch (IOException e) {
              e.printStackTrace();
              getMessageForLocalHandler(isEstablish() ? MSG_ESTABLISH_SUCCESS : MSG_FAIL, 0, 0).sendToTarget();
            }
          }
        }
      );
    }

    private volatile boolean mIsPause;

    public void pauseConnection(boolean pause) {
      mIsPause = pause;
    }

    @Override
    public void doCancel() {
      interrupt();

      mWriteThread.quit();
      mWriteThread.interrupt();

      try {
        mInputStream.close();
      } catch (IOException ignore) {
        if (Application.DEBUG) {
          Toast.makeText(Application.getInstance(), "E " + ignore.getMessage(), Toast.LENGTH_SHORT).show();
        }
      }

      try {
        mOutputStream.close();
      } catch (IOException ignore) {
        if (Application.DEBUG) {
          Toast.makeText(Application.getInstance(), "E " + ignore.getMessage(), Toast.LENGTH_SHORT).show();
        }
      }

      try {
        mPfd.close();
      } catch (IOException e) {
        e.printStackTrace();
      }

      mAccessory = null;

      if (Application.DEBUG) {
        Toast.makeText(Application.getInstance(), "CANCEL", Toast.LENGTH_SHORT).show();
      }
    }

    @Override
    protected void handleLocalMessage(Message msg) {
      switch (msg.what) {
        case MSG_FAIL:
          cancel();
          connectionLost();
          break;

        case MSG_ESTABLISH_SUCCESS:
          Proto.Event tapEvent = Helper.newTapEvent(
                  Helper.now(),
                  0,
                  0,
                  0,
                  0,
                  Proto.GestureEvent.ENDED,
                  1
          );
          write(Helper.dataFromEvent(tapEvent));

          if (!isAlive()) {
            start();
          }
          connected();
          break;

        case MSG_ESTABLISH_FAIL:
          cancel();
          connectionFail();
          break;
      }
    }
  }

  private static class WriteThread extends HandlerThread {

    private Handler mHandler = null;

    public WriteThread(String name, int priority) {
      super(name, priority);
    }

    public void prepareHandler() {
      mHandler = new Handler(getLooper());
    }

    public void runTask(Runnable runnable) {
      if (mHandler != null) {
        mHandler.post(runnable);
      }
    }

    @Override
    public boolean quit() {
      mHandler = null;
      return super.quit();
    }
  }
}
