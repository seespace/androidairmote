package tv.inair.airmote.connection;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.github.johnpersano.supertoasts.SuperActivityToast;
import com.github.johnpersano.supertoasts.SuperToast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import inair.eventcenter.proto.Helper;
import inair.eventcenter.proto.Proto;
import tv.inair.airmote.Application;
import tv.inair.airmote.WifiChooserActivity;
import tv.inair.airmote.utils.LocalHandler;
import tv.inair.airmote.utils.PowerUtils;

public final class SocketClient implements LocalHandler.CanHandleMessage {

  private static final String TAG = "SocketClient";
  public static final String STOP_MESSAGE = "STOP_SETUP";
  //private static final long USB_TIMEOUT = 30 * 1000;
  private final Context mContext;
  private final LocalHandler mLocalHandler = new LocalHandler(this);
  private boolean mUsingAoaUsb = false;

  private WeakReference<Activity> mForegroundActRef;
  private Handler mUIHandler = new Handler(Looper.getMainLooper());

  //private Handler mCheckTimeOutHandler = new Handler(Looper.getMainLooper());

  //region Description
  private boolean mUSBConnected = false;
  private boolean mIsPC = false;
  private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      switch (action) {
        case UsbManager.ACTION_USB_ACCESSORY_ATTACHED:
          switchToAoaUsbConnection();
          BaseConnection.Device newDevice = new BaseConnection.Device(Application.USB_DEVICE_NAME, "");
          newDevice.parcelable = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
          connectTo(newDevice);
          break;

        case UsbManager.ACTION_USB_ACCESSORY_DETACHED:
          try {
            Thread.sleep(400);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        case Intent.ACTION_POWER_DISCONNECTED:
          mUSBConnected = false;
          //mCheckTimeOutHandler.removeCallbacksAndMessages(null);

          if (mUsingAoaUsb) {
            disconnect(true);
          }

          if (mSettingUp && !mUsingAoaUsb) {
            Application.notify("No device connect", Application.Status.ERROR);
            stopScanAndQuickConnect();
          }

          if (mUsingAoaUsb || !isConnected()) {
            switchToWifiConnection();
            connectToDeviceIfHave(context);
          }

          if (mForegroundActRef != null && mForegroundActRef.get() instanceof WifiChooserActivity) {
            mForegroundActRef.get().finish();
          }
          break;

        // legacy wifi direct
        case Intent.ACTION_POWER_CONNECTED:
          mIsPC = PowerUtils.isConnected(context, false);
          if (!mUsingAoaUsb) {
            if (!connectToAoaIfNeed(context)) {
              mUSBConnected = true;
              quickScanAndConnect();
            }
          }

          //mCheckTimeOutHandler.postDelayed(
          //  new Runnable() {
          //    @Override
          //    public void run() {
          //      if ((mUsingAoaUsb || mUSBConnected) && !isConnected()) {
          //        mConnection.setState(BaseConnection.STATE_CONNECTION_ESTABLISH_FAILED);
          //      }
          //    }
          //  },
          //  USB_TIMEOUT
          //);
          break;
      }
    }
  };

  private SuperActivityToast mSuperActivityToast;

  public void setForegroundActivity(Activity activity) {
    if (activity != null) {
      mForegroundActRef = new WeakReference<>(activity);
    } else {
      mForegroundActRef.clear();
      mForegroundActRef = null;
    }
  }

  private volatile boolean mSettingUp = false;

  private void quickScanAndConnect() {
    if (!mIsPC || !mUSBConnected || !mSettingUp) {
      return;
    }
    Application.notify("USB Connected", Application.Status.SUCCESS);
    quickConnect();
  }

  private boolean connectToAoaIfNeed(Context context) {
    UsbManager usb = (UsbManager) context.getSystemService(Context.USB_SERVICE);
    UsbAccessory[] accessories = usb.getAccessoryList();
    if (accessories != null) {
      switchToAoaUsbConnection();
      BaseConnection.Device newDevice = new BaseConnection.Device(Application.USB_DEVICE_NAME, "");
      newDevice.parcelable = accessories[0];
      if (connectTo(newDevice)) {
        return true;
      }
    }
    return false;
  }

  public void connectToDeviceIfHave(Context context) {
    if (PowerUtils.isConnected(context, false)) {
      if ((isConnecting() || isConnected() || isConnectionEstablished()) && isUsingAoaUsb()) {
        return;
      }
      if (connectToAoaIfNeed(context)) {
        return;
      }
    }

    BaseConnection.Device prevConnectedDevice = BaseConnection.Device.getFromPref(Application.getDefaultPreferences());
    if (!isConnected() && !isConnecting()) {
      if (prevConnectedDevice != null && prevConnectedDevice.deviceName != null) {
        Log.i(TAG, "Try connect to previous connected device - device ip: " + prevConnectedDevice.address);
        connectTo(prevConnectedDevice);
      } else {
        mUIHandler.postDelayed(new Runnable() {
          @Override
          public void run() {
            quickConnect();
          }
        }, 500);
      }
    }
  }

  private void stopScanAndQuickConnect() {
    onStateChanged(false, STOP_MESSAGE);
    if (!mSettingUp) {
      updateCurrentStatus();
    }
    mConnection.stopQuickConnect();
  }

  public synchronized void changeToSettingMode(boolean setup) {
    mSettingUp = setup;
    if (!isUsingAoaUsb()) {
      if (!setup) {
        stopScanAndQuickConnect();
      } else {
        quickConnect();
      }
    }
  }

  public void switchToAoaUsbConnection() {
    disconnect(false);
    mConnection = AoaUsbAdapter.getInstance();
    mConnection.setHandler(mLocalHandler);
    mConnection.register(mContext);
    mUsingAoaUsb = true;
  }

  public void switchToWifiConnection() {
    disconnect(false);
    mConnection = WifiAdapter.getInstance();
    mConnection.setHandler(mLocalHandler);
    mConnection.register(mContext);
    mUsingAoaUsb = false;
  }

  public synchronized boolean isUsingAoaUsb() {
    return mUsingAoaUsb;
  }

  public void updateCurrentStatus() {
    if (isConnected()) {
      Application.notify("Connected to " + mDevice.deviceName, Application.Status.SUCCESS);
    } else {
      Application.notify("Disconnected", Application.Status.ERROR);
    }
  }

  public void quickConnect() {
    mConnection.startQuickConnect();
  }

  public synchronized boolean isInSettingMode() {
    return mSettingUp;
  }

  private WeakReference<Activity> activity;

  public void register(Activity context) {
    activity = new WeakReference<>(context);

    IntentFilter filter = new IntentFilter();
    filter.addAction(Intent.ACTION_POWER_CONNECTED);
    filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
    filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
    context.registerReceiver(mReceiver, filter);

    filter = new IntentFilter();
    filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
    LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver, filter);

    WifiAdapter.getInstance().register(context.getApplicationContext());
    AoaUsbAdapter.getInstance().register(context.getApplicationContext());
  }

  public void unregister() {
    LocalBroadcastManager.getInstance(Application.getInstance()).unregisterReceiver(mReceiver);
    disconnect(false);

    WifiAdapter.getInstance().register(Application.getInstance().getApplicationContext());
    AoaUsbAdapter.getInstance().register(Application.getInstance().getApplicationContext());

    if (activity != null && activity.get() != null) {
      Activity f = activity.get();
      mConnection.unregister(f);
      f.unregisterReceiver(mReceiver);
      activity = null;
    }
  }
  //endregion

  //region Public
  private BaseConnection mConnection;
  private BaseConnection.Device mDevice = BaseConnection.Device.EMPTY;

  public SocketClient(Context context) {
    mContext = context;
    switchToWifiConnection();
  }

  public void startScanInAir(BaseConnection.DeviceDiscoverListener listener) {
    WifiAdapter.getInstance().registerDeviceFoundListener(listener);
    WifiAdapter.getInstance().startScan(false);
  }

  public void stopScanInAir() {
    WifiAdapter.getInstance().registerDeviceFoundListener(null);
    WifiAdapter.getInstance().stopScan();
  }

  public void ensureNotConnectToWifiDirect() {
    mConnection.stopQuickConnect();
  }

  public boolean connectTo(BaseConnection.Device device) {
    Log.d(TAG, "ConnectTo " + device.address + " " + device.deviceName);
    if (device.deviceName != null) {
      if (mDevice != device) {
        mDevice = device;
        return mConnection.connect(device);
      }
      return false;
    } else {
      return false;
    }
  }

  public synchronized boolean isConnected() {
    return mConnection.isConnected();
  }

  public synchronized boolean isConnecting() {
    return mConnection.isConnecting();
  }

  public void disconnect(boolean all) {
    if (mConnection == null || !isConnected()) {
      return;
    }
    mDevice = BaseConnection.Device.EMPTY;
    mConnection.stop(all);
    mUsingAoaUsb = false;
    Log.d(TAG, "Disconnect");
  }

  public void sendEvent(Proto.Event e) {
    if (mConnection == null) {
      return;
    }
    if (isConnected() || isConnectionEstablished()) {
      byte[] data = Helper.dataFromEvent(e);
      mConnection.write(data);
    }
  }
  //endregion

  //region EventListener
  private List<WeakReference<OnEventReceived>> mEventReceived = new ArrayList<>();
  private List<WeakReference<OnSocketStateChanged>> mStateChanged = new ArrayList<>();

  public void addEventReceivedListener(OnEventReceived listener) {
    mEventReceived.add(new WeakReference<>(listener));
  }

  public void addSocketStateChangedListener(OnSocketStateChanged listener) {
    mStateChanged.add(new WeakReference<>(listener));
  }

  //public void removeEventReceivedListener(OnEventReceived listener) {
  //  int removedIdx = -1;
  //  for (int i = 0; i < mEventReceived.size(); i++) {
  //    if (mEventReceived.get(i).get() == listener) {
  //      removedIdx = i;
  //      break;
  //    }
  //  }
  //
  //  mEventReceived.remove(removedIdx);
  //}
  //
  //public void removeSocketStateChangedListener(OnSocketStateChanged listener) {
  //  int removedIdx = -1;
  //  for (int i = 0; i < mStateChanged.size(); i++) {
  //    if (mStateChanged.get(i).get() == listener) {
  //      removedIdx = i;
  //      break;
  //    }
  //  }
  //
  //  mStateChanged.remove(removedIdx);
  //}

  private synchronized void onEventReceived(Proto.Event e) {
    Iterator<WeakReference<OnEventReceived>> it = mEventReceived.iterator();
    while (it.hasNext()) {
      WeakReference<OnEventReceived> l = it.next();
      if (l.get() != null) {
        l.get().onEventReceived(e);
      } else {
        it.remove();
      }
    }
  }

  private synchronized void onStateChanged(boolean connect, String message) {
    Iterator<WeakReference<OnSocketStateChanged>> it = mStateChanged.iterator();
    while (it.hasNext()) {
      WeakReference<OnSocketStateChanged> l = it.next();
      if (l.get() != null) {
        l.get().onStateChanged(connect, message);
      } else {
        it.remove();
      }
    }
  }
  //endregion

  private void handleStateChange(int state) {
    switch (state) {
      case BaseConnection.STATE_CONNECTED:
        if (!mUsingAoaUsb) {
          BaseConnection.Device.saveToPref(Application.getDefaultPreferences(), mDevice);
        }

        if (activity == null) {
          return;
        }
        //if (!isUsingAoaUsb()) {
        //  Activity lActivity = activity.get();
        //  if (lActivity != null) {
        //    sendEvent(Helper.newDeviceEvent(lActivity, Proto.DeviceEvent.REGISTER));
        //  }
        //}
        onStateChanged(true, mDevice.deviceName);
        Application.notify("Connected to " + mDevice.deviceName, Application.Status.SUCCESS);
        break;

      case BaseConnection.STATE_CONNECTING:
        Application.notify("Connecting to " + mDevice.deviceName, Application.Status.NORMAL);
        break;

      case BaseConnection.STATE_NONE:
        if (!mSettingUp) {
          onStateChanged(false, mDevice.deviceName);
          Application.notify("Disconnected", Application.Status.ERROR);
        } else if (WifiAdapter.INAIR_DEVICE.deviceName.equals(mDevice.deviceName)) {
          onStateChanged(false, STOP_MESSAGE);
        }
        break;

      case BaseConnection.STATE_FAIL:
        Application.notify("Connection failed. Please try again!", Application.Status.ERROR);
        if (mUsingAoaUsb && mForegroundActRef != null && mForegroundActRef.get() != null) {
          if (mSuperActivityToast != null) {
            mSuperActivityToast.dismiss();
          }

          mSuperActivityToast = new SuperActivityToast(mForegroundActRef.get(), SuperToast.Type.BUTTON);
          mSuperActivityToast.setText("Cannot connect to your InAiR box. Please try to re-plug your USB cable.");
          mSuperActivityToast.setButtonIcon(SuperToast.Icon.Dark.EXIT, "");
          mSuperActivityToast.setDuration(SuperToast.Duration.EXTRA_LONG);
          mSuperActivityToast.setTextSize(SuperToast.TextSize.MEDIUM);
          mSuperActivityToast.setBackground(SuperToast.Background.GRAY);
          mSuperActivityToast.show();
        }

        disconnect(false);
        break;
    }
  }

  private void updateDeviceName(String name) {
    mDevice.deviceName = name;
  }

  private void updateDeviceAddress(String address) {
    mDevice.address = address;
  }

  public boolean isConnectionEstablished() {
    return mConnection.isEstablish();
  }

  @Override
  public void handleMessage(Message msg) {
    switch (msg.what) {
      case BaseConnection.Constants.MESSAGE_STATE_CHANGE:
        handleStateChange(msg.arg1);
        break;

      case BaseConnection.Constants.MESSAGE_READ:
        onEventReceived(Helper.parseFrom((byte[]) msg.obj));
        break;

      case BaseConnection.Constants.MESSAGE_DEVICE_NAME:
        // save the connected device's name
        updateDeviceName(msg.getData().getString(BaseConnection.Constants.DEVICE_NAME));
        updateDeviceAddress(msg.getData().getString(BaseConnection.Constants.DEVICE_ADDRESS));
        break;
    }
  }
}
