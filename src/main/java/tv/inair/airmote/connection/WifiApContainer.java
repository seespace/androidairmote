package tv.inair.airmote.connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguyencongtrung on 8/6/15.
 */
public class WifiApContainer {
  private static List<WifiAP> wifiAPs = new ArrayList<>();

  public static int getCount() {
    return wifiAPs.size();
  }

  public static void addAp(WifiAP ap) {
    wifiAPs.add(ap);
  }

  public static List<WifiAP> getWifiAps() {
    return wifiAPs;
  }
}
